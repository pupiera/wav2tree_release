# compute the total duration from the csv file by corpora

import sys

if __name__ =="__main__":
    path = sys.argv[1]
    corpora={}
    with open(path,"r",encoding="utf-8") as inp:
        first=True
        for line in inp:
            if first:
                first=False
                continue
            fields = line.split(",")
            corpus = fields[0].split("-")[1]
            duration = float(fields[1])
            if corpus not in corpora:
                corpora[corpus]=duration
            else:
                corpora[corpus]+=duration
    print({k: v/60 for k, v in sorted(corpora.items())})
