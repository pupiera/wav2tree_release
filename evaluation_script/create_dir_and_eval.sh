#! /bin/bash
# create the dir for the ref and hyp
# extract the data from the two conllu file by corpora and store it in the corresponding dir
# compute the metrics by corpora
if [[ $# -ne 2 ]]; then
	echo "usage : ref_conllu hyp_conllu"
	exit
fi


ref_conllu = $1
hyp_conllu = $2

mkdir ref
mkdir hyp

bash extract_all_corpus_from_conllu.sh $ref_conllu ref
bash extract_all_corpus_from_conllu.sh $hyp_conllu hyp
bash eval_all.sh ref hyp

