#! /bin/bash
# take the ref and the hyp file and output the hyp
# file in the order of the ref file based on the 
# sentence id 

import sys

order=[]
with open(sys.argv[1],"r",encoding="utf-8") as inp:
    for line in inp:
        fields=line.split()
        order.append(fields[-1])

sentence={}
with open(sys.argv[2],"r",encoding="utf-8") as inp:
    for line in inp:
        fields=line.split()
        sentence[fields[-1]] = " ".join(fields[:-1])

for o in order:
    print(f"{sentence[o]} {o}")
