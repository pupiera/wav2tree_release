# From the csv file get the relevant sentence and  extract the corresponding sentence from the gold file. 

import sys

def getSentenceListFromCSV(path_csv):
	first=True
	Slist=[]
	with open(path_csv,"r",encoding="utf-8") as fin:
		for line in fin:
			if first: # ignore first line
				first=False
				continue
			field=line.split(",")
			Slist.append(field[0]) #sent_id is first field
	return Slist

def isRelevant(line,sentencesList):
	field=line.split("=")
	if field[1].replace(" ","").replace("\n","") in sentencesList:
		return True
	else:
		return False

def writeRelevantSentence(sentencesList, path_gold,fout):
	relevant=False
	with open(path_gold,"r",encoding="utf-8") as fin:
		for line in fin:
			if line.startswith("# sent_id"):
				relevant=isRelevant(line,sentencesList)
			if relevant: #copy line
				fout.write(line)

if __name__ == "__main__":
	path_csv = sys.argv[1]
	path_gold = sys.argv[2]
	name_out = sys.argv[3]
	
	sentencesList=getSentenceListFromCSV(path_csv)
	with open(name_out,"w",encoding="utf-8") as fout:
		writeRelevantSentence(sentencesList, path_gold,fout)	
