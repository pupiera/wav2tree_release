# extract specific sentence of a corpus from a mixed corpora file
# It detect the relevant ones with the name of the corpus in the
# sent_id line

import sys

if __name__ == "__main__":
    corpus_name = sys.argv[1]
    conllu_path = sys.argv[2]
    printing=False
    with open(conllu_path, "r", encoding="utf-8") as inp:
        for line in inp:
            if line == "\n": #new sentence
                if printing:
                    print()
                printing=False
            if line.startswith("# sent_id"):
                sent_id=line.split("=")[1]
                if corpus_name in sent_id:
                    printing=True
            if printing:
                print(line,end='')
