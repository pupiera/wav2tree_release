import sys

if __name__ == "__main__":
    corpus_file = sys.argv[1]
    data={}
    first=True
    with open(corpus_file, "r",encoding="utf-8") as inp:
        for line in inp:
            if first:
                first=False
                continue
            fields = line.split(",")#csv separator
            corpus_name = fields[0].split("-")[1]
            if corpus_name in data:
                data[corpus_name]+=1
            else:
                data[corpus_name]=1
    for key in sorted(data.keys()):
        print(f"{key} : {data[key]}")

