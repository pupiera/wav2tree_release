refDir=$1
hypDir=$2
corpus_list='cfpb cfpp clapi coralrom crfp fleuron frenchoral ofrom reunions tcof tufs valibel'
rm result_wer
rm detail_wer
for c in $corpus_list; do
    python3 eval_WER.py $refDir/trans/$c.trans $hypDir/trans/$c.trans False >> result_wer 2>> detail_wer
done
