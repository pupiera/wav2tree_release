#! /bin/bash
# for each corpora in the list compute the metrics (UAS, LAS)
# the file in the two directory need to have the same name to be matched
# and are generated from the extract_all.sh script


if [ $# -ne 2 ]; 
then
	echo "usage : bash eval_all.sh DirRef DirHyp"
	exit
fi

dirRef=$1
dirHyp=$2

corpus_list='cfpb cfpp clapi coralrom crfp fleuron frenchoral ofrom reunions tcof tufs valibel'
for c in $corpus_list; do
	echo result_$c
	bash eval_align.sh $dirRef/$c $dirHyp/$c >result_$c 
done
