# create subDataset from wer value. 
import sys

def read_werfile(werpath):
    wer_dict={}
    with open(werpath, "r", encoding="utf-8") as inp:
        for line in inp:
            fields = line.split()
            sent_id = fields[0].replace("(","").replace(")","")
            wer_dict[sent_id] = fields[1]
    return wer_dict
def read_conllu(conllu):
    conllu_dict ={}
    with open(conllu, "r", encoding ="utf-8") as inp:
        sent=[]
        sent_id=None
        for line in inp:
            if line.startswith("# sent_id"):
                sent_id = line.split("=")[-1].replace(" ","").replace("\n","")
                continue
            if line == "\n":
                conllu_dict[sent_id]=sent
                sent=[]
                sent_id=None
                continue
            sent.append(line)
    return conllu_dict

if __name__ == '__main__':
    conllu_file = sys.argv[1]
    wer_file = sys.argv[2]
    wer_d = read_werfile(wer_file)
    conllu_d = read_conllu(conllu_file)
    for wer in range(10,101,10):
        with open(f"{conllu_file}_wer{wer}","w",encoding="utf-8") as out:
            for key, item in wer_d.items():
                print(item)
                if float(item) > wer:
                    continue
                conll = conllu_d[key]
                out.write(f"#sent_id = {key}\n")
                for line in conll:
                    out.write(line)
                out.write("\n")

