#! /bin/bash
# For all the corpora (in corpus_list), extract the corresponding conllu sentence to $extracted directory
if [[ $# -ne 2 ]]; then
	echo "usage : bash extract_all_corpus_from_conllu.sh conllu_path extractedDirectory"
	exit
fi

conllu_path=$1
extracted=$2
corpus_list='cfpb cfpp clapi coralrom crfp fleuron frenchoral ofrom reunions tcof tufs valibel'
mkdir -p $extracted
for c in $corpus_list; do
	python3 extract_corpus_from_conllu.py $c $conllu_path > $extracted/$c
done
