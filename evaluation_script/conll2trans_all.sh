#! /bin/bash
# For each conllu file named by the corpora name, extract the correponding transcription
dir=$1
corpus_list='cfpb cfpp clapi coralrom crfp fleuron frenchoral ofrom reunions tcof tufs valibel'
mkdir -p trans
for c in $corpus_list; do
    python3 conll2trans.py $dir/$c trans/$c.trans 
done
