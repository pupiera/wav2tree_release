#! /bin/bash
# This script automatically generate the sclite file used for the evaluation
# just replace the path of the python script to your path

if [[ $# -ne 2 ]]; then
	echo "eval_align.sh usage : bash eval_align.sh ref hyp"
	exit
fi

ref=$1 #conllu
hyp=$2 #conllu
python3 ~/utils_script/conll2trans.py $hyp hyp_trans 
python3 ~/utils_script/conll2trans.py $ref ref_trans

sclite -F -i wsj -r ref_trans -h hyp_trans -o sgml

#rm ref_trans
#rm hyp_trans

python3 ../src/eval/conll18_ud_eval.py --verbose $ref $hyp hyp_trans.sgml
rm hyp_trans.sgml

