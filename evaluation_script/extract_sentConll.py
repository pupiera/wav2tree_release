# extract the sentence of one file from the the sentence present in another one
# Make it so the two file have exactly the same sentence (should not be needed)

import sys

def get_sent(conll):
    sent_ids=[]
    with open(conll, "r", encoding="utf-8") as inp:
        for line in inp:
            if "sent_id" in line:
                sent_ids.append(line.split("=")[-1].replace("\n","").replace(" ",""))
    return sent_ids
if __name__ == "__main__":
    to_extract_file = sys.argv[1]
    sent_file = sys.argv[2]
    sent_ids = get_sent(sent_file)
    with open(to_extract_file, "r", encoding="utf-8") as inp:
        is_write=False
        for line in inp:
            if line.startswith("# sent_id"):
                sent_id = line.split("=")[-1].replace("\n","").replace(" ","")
                if sent_id in sent_ids:
                    print(line,end='')
                    is_write = True
                continue 
            if is_write:
                print(line, end='')
            if line =="\n":
                is_write=False

            
