# Add sent_id to file where the sent_id was deleted
# Need the original file to add it and the sentence must be in the same order
# The first sentence of the hyp file will have the same sent_id as the first sentence 
# of the ref file.

import sys

def readRef(ref):
    sent_id=[]
    with open(ref, "r", encoding="utf-8") as inp:
        for line in inp:
            if "sent_id" in line:
                sent_id.append(line)
    return sent_id

if __name__ == "__main__":
    ref = sys.argv[1]
    hyp = sys.argv[2]
    sent_id = readRef(ref)
    print(len(sent_id), file=sys.stderr)
    with open(hyp, "r", encoding="utf-8") as inp:
        print(sent_id[0],end='')
        i=1
        for line in inp:
            if line =='\n' and i < len(sent_id):
                print(line,end='')
                print(sent_id[i],end='')
                i+=1
                continue
            print(line,end='')
