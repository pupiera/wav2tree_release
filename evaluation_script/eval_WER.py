# From the gold conllu file and the hyp conlluf file, compute
# the Word error rate with the speechbrain module
# output the wer by utterance and the mean WER of the corpus

from speechbrain.utils.edit_distance import wer_details_by_utterance
import sys
if __name__ == "__main__":
    ref = sys.argv[1]
    hyp = sys.argv[2]
    WER_in_file = sys.argv[3] == "True" 
    sent_gold={}
    sent_ASR={}

    with open(ref, "r", encoding="utf-8") as inp:
        for line in inp:
            fields=line.split()
            sent_gold[fields[-1]]=[x.lower() for x in fields[:-1]]
    with open(hyp, "r", encoding="utf-8") as inp:
        for line in inp:
            fields=line.split()
            if WER_in_file:
                sent_ASR[fields[-2]]=[x.lower() for x in fields[:-2]]
            else:
                sent_ASR[fields[-1]]=[x.lower() for x in fields[:-1]]
    #print(sent_gold)
    #print(sent_ASR)
    ASR_WER={}
    ASR_WER = wer_details_by_utterance(sent_gold, sent_ASR)
    #print(ASR_WER)
    for a in ASR_WER:
        print(f"{a['key']} {a['WER']}",file=sys.stderr)
    print(f"{ref} {hyp} wer : {sum([x['WER'] for x in ASR_WER])/len(ASR_WER)}")
    print(f"{ref} {hyp} (Normalized) : {wer_summary(ASR_WER)['WER']}")


