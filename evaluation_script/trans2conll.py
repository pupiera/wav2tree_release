# From the transcriptions and the gold conllu file
# generate empty conllu file ( number + asr words)
# conllu file is only there for the order constraint
import sys

trans_file = sys.argv[1]
conllu_file= sys.argv[2]
wer = sys.argv[3] == "True"
order=[]
with open(conllu_file,"r", encoding="utf-8") as inp:
    for line in inp:
        if line.startswith("# sent_id"):
            fields=line.split("=")
            order.append(fields[1].replace("\n","").replace(" ",""))


data={}
with open(trans_file, "r", encoding="utf-8") as inp:
    for line in inp:
        fields = line.split()
        if wer :
            words = fields[:-2]
            sent_id = fields[-2].replace("(","").replace(")","").replace(" ","")
        else:
            words = fields[:-1]
            sent_id = fields[-1].replace("(","").replace(")","").replace(" ","")
        data[sent_id]=words

for o in order:
    words = data[o]
    print(f"# sent_id = {o}")
    for i, w in enumerate(words):
        print(f"{i+1}\t{w}")
    print()
