# WAV2TREE

## Description

Implementation of the "End-to-End dependecy parsing of spoken French" Paper.
This architecture is able to parse speech to compute the transcriptions of a given wav file and compute the corresponding dependency tree from the raw signal.

## Installation

1. Install a 3.9.7 python environment, with [virtualenv](https://docs.python.org/3/library/venv.html) or [conda](https://docs.conda.io/en/latest/).
2. Clone this repository: `git clone https://gricad-gitlab.univ-grenoble-alpes.fr/pupiera/wav2tree_release`
3. Install speechBrain: https://github.com/speechbrain/speechbrain 
    ```
     git clone https://github.com/speechbrain/speechbrain 
     pip install -e speechbrain
    ```
4. Install all python dependency: `pip install -r requirement.txt`
5. Install sclite from instructions: https://github.com/usnistgov/SCTK
    - Update your bash path with the executable : 
    ```
    EXPORT PATH = "/home/Sclite/Path/...:$PATH"
    ```

## Usage

To reproduce our result : 

1. Download Orfeo via the website (https://www.ortolang.fr/market/corpora/cefc-orfeo) version 1.5 or via command line : 
    ```
    wget --header 'Host: repository.ortolang.fr' --header 'Sec-Fetch-Site: same-site' 'https://repository.ortolang.fr/api/content/export?&path=/cefc-orfeo/11/&filename=cefc-orfeo&scope=YW5vbnltb3Vz3' --output-document 'cefc-orfeo.zip'
    ```
2. Split wav files and prepare data with this script: `wav2tree_release/utils_data/splitWavOnTimestamp.py`
    ```
    python splitWavOnTimestamp.py <path_to_CEFC-ORFEO>/11/oral/ 
    ```
3. Edit the path value in the given CSV files (train, dev, test) to the corresponding files in your environment. 
    The CSV files to edit are in `results/<exp_name>/1234/save/` You can do so automatically by running: 
    ```bash
    sed -i 's/\/home\/getalp\/data\/ASR_data\/FR\/CORPUS_AUDIO\/cefc-orfeo_v.1.5_december2021/<PATH>/g' src/results/*/1234/save/*.csv
    ```
    where `<PATH>` is the path to the `cefc-orfeo/` folder with escaped `'/'` characters.
4. Edit `PATH_TO_SCLITE` and `dir_gold_conllu` in the hparams yaml file for your environment.
5. run: `python wav2tree.py hparams/xxx.yaml`

## Evaluate your result

Once the train has finished, you should have some file named : test_Audio.conllu. This is the output of the model from the test csv file. To compute the metrics, you need to use the specified script : (This is a modified version of the conllu_2018 evaluation script which take the 2 conllu file and the output of sclite to compute the metrics)
```
bash eval_align.sh test_gold.conllu test_Audio.conllu
```

Computing the metrics by corpora (UAS, LAS) : 
```
bash create_dir_and_eval.sh utils_data/conllu/test_gold.conllu test_Audio.conllu 
```
(note that this command create 3 file per corpora, 1 in two subdirectory and 1 in the current dir)

Computing WER for each corpora (It will create a directory with the transcriptions of the differents corpora): 
```
bash conll2trans_all.sh refConlluDir
bash conll2trans_all.sh hypConlluDir
```
```
bash compute_all_wer.sh ref_transcription_dir hyp_transcription_dir
```

## Result

This table illustrate the result of the wav2tree approach.

The first part of the table (form corpus to the first LAS) is with wav2tree.

The second part (from second WER to second LAS) is with a pipeline baseline (same number of parameters) transcription then parsing.

Finally the third part (from third WER to last LAS) is with a pipeline base but using the oracle described in the article.

| Corpus         | WER           | CER  | UPOS          | UAS           | LAS           | WER           | CER  | UPOS | UAS  | LAS           | WER           | CER  | UPOS          | UAS           | LAS  |
|----------------|---------------|------|---------------|---------------|---------------|---------------|------|------|------|---------------|---------------|------|---------------|---------------|------|
| Cfpb           | 29.2          | 18.0 | 77.6          | **73.4**   | **68.8** | **28.2** | 17.0 | 76.6 | 71.8 | 67.5          | **28.2** | 17.0 | **78.0** | 73.0          | 68.5 |
| Cfpp           | 35.7          | 24.2 | **71.8** | **66.6** | **61.8** | **35.5** | 24.2 | 69.9 | 64.8 | 60.2          | **35.5** | 24.2 | 70.7          | 65.0          | 60.4 |
| Clapi          | 53.6          | 35.4 | **58.8** | **54.4** | **47.6** | **53.2** | 35.0 | 56.4 | 53.5 | 46.7          | **53.2** | 35.0 | 57.4          | 53.75         | 47.0 |
| Coralrom       | 22.5          | 11.9 | **83.9** | **77.8** | **74.5** | **22.0** | 11.8 | 82.0 | 75.3 | 71.8          | **22.0** | 11.8 | 83.0          | 75.9          | 72.4 |
| Crfp           | 24.3          | 14.5 | **81.7** | **75.9** | **72.2** | **23.5** | 14.3 | 80.3 | 74.0 | 70.4          | **23.5** | 14.3 | 81.3          | 74.8          | 71.1 |
| Fleuron        | 36.1          | 23.1 | 71.9          | 65.3          | 60.5          | **35.5** | 21.6 | 71.0 | 65.1 | **61.3** | **35.5** | 21.6 | **72.1** | **66.0** | 61.1 |
| Oral-Narrative | 11.1          | 4.7  | **93.2** | **87.9** | **85.7** | **10.2** | 4.3  | 92.1 | 86.1 | 83.4          | **10.2** | 4.3  | 93.0          | 86.6          | 84.2 |
| Ofrom          | 20.0          | 11.6 | **85.2** | **79.3** | **75.9** | **19.1** | 11.2 | 84.2 | 77.9 | 74.7          | **19.1** | 11.2 | 85.1          | 78.4          | 75.1 |
| Reunions       | **40.9** | 26.1 | **67.7** | **61.8** | **56.3** | 41.3          | 26.0 | 65.5 | 60.3 | 55.1          | 41.3          | 26.0 | 66.7          | 60.8          | 55.6 |
| Tcof           | 34.1          | 20.8 | **74.3** | **67.4** | **62.7** | **33.6** | 20.4 | 72.3 | 65.4 | 60.8          | **33.6** | 20.4 | 73.2          | 65.6          | 61.0 |
| Tufs           | 33.1          | 20.8 | **75.2** | **69.6** | **65.1** | **32.5** | 20.3 | 73.5 | 67.8 | 63.5          | **32.5** | 20.3 | 74.6          | 68.7          | 64.1 |
| Valibel        | 23.0          | 12.8 | **82.8** | **76.9** | **73.2** | **22.3** | 12.5 | 81.3 | 75.4 | 71.7          | **22.3** | 12.5 | 82.2          | 75.7          | 71.8 |
| Orféo full     | 31.0          | 19.4 | **77.4** | **71.7** | **67.5** | **29.1** | 17.9 | 75.8 | 70.0 | 65.8          | **29.1** | 17.9 | 76.7          | 70.5          | 66.2 |



## Gold and silver data breakdown

| Corpus      | Gold tokens |Total tokens | Share of gold |
| ----------- | ----------- |----------- |----------- |
| cfpb      | 0       |58688       |0       |
| cfpp   | 0        |494737        |0        |
| clapi      | 32630       |155455       |0.21       |
| coralrom   | 5506        |244671        |0.02        |
| crfp      | 0       |398667       |0       |
| fleuron   | 0        |32571        |0        |
| french-Oral-Narrative      | 0       |145395       |0       |
| ofrom   | 0        |282687        |0        |
| reunion-de-travail      | 0       |210991       |0       |
| tcof   | 122665        |374276        |0.33        |
| tufs      | 0       |720158       |0       |
| valibel   | 11818        |461442        |0.03        |
| ALL   | 172619        |3579738        |0.05        |


## Pretrained models

Pre trained models on Orféo and French Oral Narrative available here : https://drive.google.com/drive/folders/1hzpUWPn_pOek70xPcigcrvs_13oZsZWF?usp=sharing

To use them, just copy paste the checkpoint folder to the save folder of your experiment. (results/Name_EXP/seed/save/)

## Authors and acknowledgment

This project was developed under the funding of ANR PROPICTO (ANR-20-CE93-0005).

## License

Shield: [![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg

