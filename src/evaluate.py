import torch
import dep2label.encodings
import postprocessing
class dep2LabelDecoder:
    '''
    Class taking probabilities of each label and the alphabet and converting it to conll text.
    '''

    def __init__(self,alphabet):
        self.gov_alphabet = alphabet[0]
        self.dep_alphabet = alphabet[1]
        self.pos_alphabet = alphabet[2]
        self.reverse_gov_alphabet = {value : key for (key, value) in self.gov_alphabet.items()}
        self.reverse_dep_alphabet = {value : key for (key, value) in self.dep_alphabet.items()}
        self.reverse_pos_alphabet = {value : key for (key, value) in self.pos_alphabet.items()}
        self.encoding = dep2label.encodings.RelPosEncoding()
        self.l = postprocessing.LabelPostProcessor()
        self.p = postprocessing.CoNLLPostProcessor()
        self.decoded_sentences={}
        self.nb_of_sentence=1
    def decode(self,p_depLabel,p_govLabel,predicted_words,p_posLabel,sent_ids):
        '''
            Decode the probability into a dependency tree
        Parameters
        ----------
        p_depLabel : tensor of form [batch,seq_len,len(self.dep_alphabet)]
        p_govLabel : tensor of form [batch,seq_len,len(self.gov_alphabet)]
        predicted_words : list of list of predicted words by ASR.
        Returns: For each element of the batch (sentence) a list of string, where each element correspond to a line
        in a conllu format.
        -------
        '''

        for p_deps, p_govs,words,p_poss,sent_id in zip(p_depLabel,p_govLabel,
                predicted_words,p_posLabel,sent_ids): #for each element of the batch
            to_decode={0: ["-BOS-",	"-BOS-","-BOS-","-BOS-","-BOS-"]}
            for i,(p_dep, p_gov,p_pos, word) in enumerate(zip(p_deps, p_govs,p_poss,words)): # for each element of seqlen
                dep=self.reverse_dep_alphabet[torch.argmax(p_dep).item()]
                gov=self.reverse_gov_alphabet[torch.argmax(p_gov).item()].split("@")
                pos=self.reverse_pos_alphabet[torch.argmax(p_pos).item()]
                if word=="":
                    word="[EMPTY_ASR_WRD]"
                to_decode[i+1]=[word,pos,gov[0],dep,gov[-1]]
            #to_decode[len(to_decode)]=["-EOS-",	"-EOS-","-EOS-","-EOS-","-EOS-"]
            #print(to_decode)
            decoded_words,headless_words = self.encoding.decode(to_decode)
            # POSTPROCESSING
            if not self.l.has_root(decoded_words):
                self.l.find_candidates_for_root(decoded_words, headless_words)
            if not self.l.has_root(decoded_words):
                self.l.assign_root_index_one(decoded_words, headless_words)
            if self.l.has_multiple_roots(decoded_words):
                self.l.choose_root_from_multiple_candidates(decoded_words)
            self.l.assign_headless_words_to_root(decoded_words, headless_words)
            self.l.find_cycles(decoded_words)
            self.p.convert_labels2conllu(decoded_words)
            self.decoded_sentences.update({sent_id: decoded_words})
            self.nb_of_sentence += 1

    def writeToCoNLLU(self,pathFile,order):
        '''
        Write the tree stored through the decode function to the pathfile with a specific order
        Parameters
        ----------
        pathFile
        order

        Returns
        -------

        '''
        self.p.write_to_file(self.decoded_sentences,pathFile,order)

    def evaluateCoNLLU(self, goldPath, predictedPathFile,sgml_path):
        return self.p.evaluate_dependencies(goldPath, predictedPathFile, sgml_path)
