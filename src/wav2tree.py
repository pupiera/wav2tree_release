#!/usr/bin/env python3
import sys
import torch

import speechbrain as sb
import torchaudio
from hyperpyyaml import load_hyperpyyaml
from speechbrain.tokenizers.SentencePiece import SentencePiece
from speechbrain.utils.data_utils import undo_padding
from speechbrain.utils.distributed import run_on_main

from SPB_custom import CTC_decode as ctd
from torch.nn.utils.rnn import pad_sequence
import os
#--------------------------- Experiemnt architecture -----------------------------------#
from abstract_ASR import ABSTRACT_ASR

# --------------------------import dep2label---------------------------------------------#
from dep2label.encodings import RelPosEncoding
import evaluate
from conll2trans import conll2trans
from DynamicOracle import DynamicOracle
#--------------------------------------- Alignment import --------------------------------#
import subprocess
import tempfile
# ----------------------------------------- Import profiling -------------------------------------------#
import wandb
import math
"""Recipe for training a sequence-to-sequence ASR system with Orfeo.
The system employs a wav2vec2 encoder and a ASRDep2Label decoder.
Decoding is performed with greedy decoding (will be extended to beam search with language model).

To run this recipe, do the following:
> python train_with_wav2vec2.py hparams/train_with_wav2vec2.yaml

With the default hyperparameters, the system employs a pretrained wav2vec2 encoder.
The wav2vec2 model is pretrained following the model given in the hprams file.
It may be dependent on the language.

Authors
 * Titouan Parcollet 2021 for ASR template
 * PUPIER Adrien 2022 for adapting template to dependency parsing.
"""

INTRA_EPOCH_CKPT_FLAG = "brain_intra_epoch_ckpt"
# -----------------------------------------------------------BRAIN ASR---------------------------------------------#
# Define training procedure
class ASR(ABSTRACT_ASR):
    '''
    This is a subclass of Brain in speechbrain.
    Contains the definition/ behaviour of the model (forward + loss computation)
    '''
    def compute_forward(self, batch, stage):
        '''
        This function compute the forward pass of batch.
        It uses the Dep2Label paradigm for dependency parsing

        Parameters
        ----------
        batch : the corresponding row of the CSV file. ( contains the value defined in the pipeline)
        ["id", "sig", "tokens_bos", "tokens_eos", "tokens", "wrd", "pos_tensor", "govDep2label", "depDep2Label"]
        stage : TRAIN, DEV, TEST. Allow for specific computation based on the stage.

        Returns
        p_ctc : The probability of a given character in this frame [batch, time, char_alphabet]
        wav_lens : the lentgh of the wav file
        p_depLabel : the probablity of the syntactic functions [batch, max(seq_len), dep_alphabet]
        p_govLabel : the probablity of the head [batch, max(seq_len), gov_alphabet]
        p_posLabel : the probablity of the part of speech : [batch, max(seq_len), POS_alphabet]
        seq_len : the length of each dependency parsing element of the batch (audio word embedding lentgh)
        -------

        '''
        batch = batch.to(self.device)
        wavs, wav_lens = batch.sig
        tokens_bos, _ = batch.tokens_bos
        wavs, wav_lens = wavs.to(self.device), wav_lens.to(self.device)
        if stage == sb.Stage.TRAIN:
            if hasattr(self.hparams, "augmentation"):
                wavs = self.hparams.augmentation(wavs, wav_lens)

        # Forward pass ASR
        feats = self.modules.wav2vec2(wavs)
        x = self.modules.enc(feats)  # [batch,time,1024]
        logits = self.modules.ctc_lin(x)  # [batch,time,76]
        p_ctc = self.hparams.log_softmax(logits)  # [batch,time,76] (76 in output neurons corresponding to BPE size)
        # Forward pass dependency parsing
        if self.is_training_syntax:
            sequence, mapFrameToWord = ctd.ctc_greedy_decode(
                p_ctc, wav_lens, blank_id=self.hparams.blank_index
            )
            input_dep, seq_len = self._create_inputDep(x, mapFrameToWord)
            input_dep = input_dep.to(self.device)
            seq_len = seq_len.to(self.device)
            batch_len = input_dep.shape[0]
            hidden = torch.zeros(4, batch_len, 800, device=self.device)  # init hidden to zeros for each sentence
            cell = torch.zeros(4, batch_len, 800, device=self.device)
            lstm_out, _ = self.modules.dep2LabelFeat(input_dep, (hidden, cell))  # Prend une sequence d'embedding bert
            logits_posLabel = self.modules.posDep2Label(lstm_out)
            p_posLabel = self.hparams.log_softmax(logits_posLabel)
            logits_depLabel = self.modules.depDep2Label(lstm_out)
            p_depLabel = self.hparams.log_softmax(logits_depLabel)
            logits_govLabel = self.modules.govDep2Label(lstm_out)
            p_govLabel = self.hparams.log_softmax(logits_govLabel)
            return p_ctc, wav_lens, p_depLabel, p_govLabel, p_posLabel, seq_len
        return p_ctc, wav_lens

    def _create_inputDep(self, x, mapFrameToWord):
        '''
        Compute the word audio embedding
        Parameters
        ----------
        x : The encoder representation output ( 1 frame per 20ms with wav2vec )
        mapFrameToWord : The mapping of frame to word from the CTC module.

        Returns
        batch : The padded batch of word audio embedding [batch, max(seq_len)]
        seq_len : the length of each element of the batch
        -------

        '''
        batch = []
        hidden_size = self.hparams.repFusionHidden
        is_bidirectional = self.hparams.repFusionBidirectional
        n_layers = self.hparams.repFusionLayers
        nb_hidden = n_layers * (1 + is_bidirectional)
        for i, (rep, map) in enumerate(zip(x, mapFrameToWord)):  # for 1 element on the batch do :
            map = torch.Tensor(map)
            uniq = torch.unique(map)
            fusionedRep = []
            hidden = torch.zeros(nb_hidden, 1, hidden_size,
                                 device=self.device)  # init hidden to zeros for each sentence
            cell = torch.zeros(nb_hidden, 1, hidden_size, device=self.device)  # init hidden to zeros for each sentence
            for e in uniq:  # For each word find the limit of the relevant sequence of frames
                if e.item() == 0 and len(uniq) >1: #ignore 0, if empty tensor, try with everything (i.e correspond to transition of words)
                    continue
                relevant_column = (map == e).nonzero(as_tuple=False)
                min = torch.min(relevant_column)
                max = torch.max(relevant_column)
                frames = rep[min:max + 1, :].unsqueeze(
                    0)  # should not break autograd https://discuss.pytorch.org/t/select-columns-from-a-batch-of-matrices-by-index/85321/3
                _, (hidden, cell) = self.modules.RepFusionModel(frames, (
                    hidden, cell))  # extract feature from all the relevant audio frame representation
                if is_bidirectional:
                    fusionedRep.append(torch.cat((hidden[-2], hidden[-1]), dim=1))
                else:
                    fusionedRep.append(hidden[-1])
            batch.append(torch.stack(fusionedRep))
            # print(f"Last tensor shape : {batch[-1].shape}")
        seq_len = [len(e) for e in batch]
        batch = torch.reshape(pad_sequence(batch, batch_first=True),
                              (len(mapFrameToWord), -1, hidden_size * (1 + is_bidirectional)))
        return batch, torch.Tensor(seq_len)

    def compute_objectives(self, predictions, batch, stage):
        '''
        This function compute the loss of our model
        Parameters
        ----------
        predictions : contains the various predictions from the forward function ( p_ctc, wav_lens, p_depLabel, p_govLabel, p_posLabel, seq_len)

        batch : the corresponding row of the CSV file. ( contains the value defined in the pipeline)
        ["id", "sig", "tokens_bos", "tokens_eos", "tokens", "wrd", "pos_tensor", "govDep2label", "depDep2Label"]

        stage : TRAIN, DEV, TEST. Allow for specific computation based on the stage.

        Returns
        -------

        '''
        if self.is_training_syntax:
            p_ctc, wav_lens, p_depLabel, p_govLabel, p_posLabel, seq_len = predictions
        else:
            p_ctc, wav_lens = predictions
        ids = batch.id

        tokens_eos, tokens_eos_lens = batch.tokens_eos
        tokens, tokens_lens = batch.tokens
        loss_ASR = self.hparams.ctc_cost(p_ctc, tokens, wav_lens, tokens_lens)
        if self.is_training_syntax:
            # the classification loss is not good enough for this in the case of segmentation error.
            # To do: Dynamic oracle based on the segmentation. ie: find best tree from gold with the number
            # of token proposed by the segmentation.
            dep2Label_dep = batch.depDep2Label[0]  # get tensor from padded data class
            dep2Label_gov = batch.govDep2label[0]
            gold_POS = batch.pos_tensor[0]
            sequence = sb.decoders.ctc_greedy_decode(
                p_ctc, wav_lens, blank_id=self.hparams.blank_index
            )
            predicted_words = self.tokenizer(sequence, task="decode_from_list")
            for i, sent in enumerate(predicted_words):
                for w in sent:
                    if w == '':
                        predicted_words[i] = [w for w in sent if w !='']
                        if len(predicted_words[i]) == 0:
                            predicted_words[i].append('EMPTY_ASR')
            target_words = undo_padding(tokens, tokens_lens)
            target_words = self.tokenizer(target_words, task="decode_from_list")
            try:
                allowed = 0 #No len mismatch beetwen pred and target. Using sclite alignment 
                            #and dynamic oracle based on the segmentation of the ASR predictions 
                            # 1 is allowed in case of inerstion of 1 empty string at the end (happen rarely)
                hyp_file = tempfile.NamedTemporaryFile()
                ref_file = tempfile.NamedTemporaryFile()
                with open(hyp_file.name,"w") as hyp, open(ref_file.name, "w") as ref:
                    for sentence, id in zip(predicted_words,ids):
                        hyp.write(f"{' '.join(sentence)} ({id}) \n")
                    for sentence, id in zip(target_words,ids):
                        ref.write(f"{' '.join(sentence)} ({id}) \n")
                #sclite_command = ["sclite", "-F", "-i", "wsj", "-r", ref_file.name,
                #                  "-h", hyp_file.name, "-o", "stdout", "sgml"
                #    , "-O", "/home/getalp/pupiera/thesis/endtoend_asr_multitask/src/ASRDEP2LABEL/"]
                sclite_command = ["sclite", "-F", "-i", "wsj", "-r", ref_file.name,
                                  "-h", hyp_file.name, "-o", "stdout", "sgml"]
                sclite_out = subprocess.run(sclite_command, capture_output=True, text=True)
                hyp_file.close()
                ref_file.close()
                dep2Label_gov, dep2Label_dep, gold_POS = \
                    self.d_oracle.findBestTreeWithSeg(sclite_out.stdout,dep2Label_gov.tolist(),dep2Label_dep.tolist(),gold_POS.tolist())
                dep2Label_gov = pad_sequence(dep2Label_gov, batch_first=True).to(self.device)
                dep2Label_dep = pad_sequence(dep2Label_dep, batch_first=True).to(self.device)
                gold_POS = pad_sequence(gold_POS, batch_first=True).to(self.device)
                try:
                    loss_depLabel = self.hparams.depLabel_cost(p_depLabel, dep2Label_dep,
                                                           length=seq_len, allowed_len_diff=allowed)
                    loss_govLabel = self.hparams.govLabel_cost(p_govLabel, dep2Label_gov,
                                                           length=seq_len, allowed_len_diff=allowed)
                    loss_POS = self.hparams.posLabel_cost(p_posLabel, gold_POS,
                                                  length=seq_len, allowed_len_diff=allowed)
                except (ValueError, RuntimeError) as e:
                    print(dep2Label_gov)
                    print(dep2Label_dep)
                    print(gold_POS)
                    self.print_tree(dep2Label_gov, dep2Label_dep, gold_POS, predicted_words)
                    raise ValueError() from e
            except ValueError as e:
                print(f" True shape : {dep2Label_dep.shape}, pred shape: {p_depLabel.shape}")
                raise ValueError() from e

            loss = loss_ASR * 0.4 \
                   + loss_depLabel * 0.2 \
                   + loss_govLabel * 0.2 \
                   + loss_POS * 0.2
        else:
            loss = loss_ASR

        if stage != sb.Stage.TRAIN:
            sequence = sb.decoders.ctc_greedy_decode(
                p_ctc, wav_lens, blank_id=self.hparams.blank_index
            )
            predicted_words = self.tokenizer(sequence, task="decode_from_list")
            target_words = undo_padding(tokens, tokens_lens)
            target_words = self.tokenizer(target_words, task="decode_from_list")

            self.wer_metric.append(ids, predicted_words, target_words)
            self.cer_metric.append(ids, predicted_words, target_words)
            if self.is_training_syntax:
                self.Evaluator.decode(p_depLabel, p_govLabel, predicted_words, p_posLabel, ids)
        if math.isnan(loss):
            print("------------")
            print(loss_ASR)
            print(loss_govLabel)
            print(dep2Label_dep)
            print(loss_depLabel)
            print(loss_POS)
        return loss

    def print_tree(self, govs, deps, poss, pred_words):
        '''
        This a debug function used to print the tree from the different tensor
        Parameters
        ----------
        govs : The tensor of governor/head
        deps : The tensor containing the syntactic funtions label
        poss : The tensor containing the Part of Speech label
        pred_words : The tensor containing the predicted words

        Returns
        -------

        '''
        for gov_tensor, dep_tensor, pos_tensor, pred_word_tensor in zip(govs, deps, poss, pred_words):
            print("--------------------------------------------------")
            for gov, dep, pos, pred_word in zip(gov_tensor, dep_tensor, pos_tensor, pred_word_tensor):
                print(f"{pred_word}\t{reverse_label_alphabet[0].get(gov.item())}\t"+
                        f"{reverse_label_alphabet[1].get(dep.item())}\t{reverse_label_alphabet[2].get(pos.item())}")



# Define custom data procedure
def dataio_prepare(hparams, tokenizer):
    """This function prepares the datasets to be used in the brain class.
    It also defines the data processing pipeline through user-defined functions."""

    # 1. Define datasets
    data_folder = hparams["data_folder"]

    train_data = sb.dataio.dataset.DynamicItemDataset.from_csv(
        csv_path=hparams["train_csv"], replacements={"data_root": data_folder},
    )

    if hparams["sorting"] == "ascending":
        # we sort training data to speed up training and get better results.
        train_data = train_data.filtered_sorted(
            sort_key="duration",
            key_max_value={"duration": hparams["avoid_if_longer_than"]},
        )
        # when sorting do not shuffle in dataloader ! otherwise is pointless
        hparams["dataloader_options"]["shuffle"] = False

    elif hparams["sorting"] == "descending":
        train_data = train_data.filtered_sorted(
            sort_key="duration",
            reverse=True,
            key_max_value={"duration": hparams["avoid_if_longer_than"]},
        )
        # when sorting do not shuffle in dataloader ! otherwise is pointless
        hparams["dataloader_options"]["shuffle"] = False

    elif hparams["sorting"] == "random":
        pass

    else:
        raise NotImplementedError(
            "sorting must be random, ascending or descending"
        )

    valid_data = sb.dataio.dataset.DynamicItemDataset.from_csv(
        csv_path=hparams["valid_csv"], replacements={"data_root": data_folder},
    )
    # We also sort the validation data so it is faster to validate
    valid_data = valid_data.filtered_sorted(sort_key="duration")

    test_data = sb.dataio.dataset.DynamicItemDataset.from_csv(
        csv_path=hparams["test_csv"], replacements={"data_root": data_folder},
    )

    # We also sort the validation data so it is faster to validate
    test_data = test_data.filtered_sorted(sort_key="duration")

    datasets = [train_data, valid_data, test_data]

    # 2. Define audio pipeline:
    @sb.utils.data_pipeline.takes("wav")
    @sb.utils.data_pipeline.provides("sig")
    def audio_pipeline(wav):
        '''
        Audio Pipeline
        Parameters
        ----------
        wav : the wav file path

        Returns
        resampled : return the raw signal from the file with the right sampling ( 16Khz)
        -------

        '''
        info = torchaudio.info(wav)
        sig = sb.dataio.dataio.read_audio(wav)
        resampled = torchaudio.transforms.Resample(
            info.sample_rate, hparams["sample_rate"],
        )(sig)
        return resampled

    sb.dataio.dataset.add_dynamic_item(datasets, audio_pipeline)

    # 3. Define text pipeline:
    @sb.utils.data_pipeline.takes("wrd")
    @sb.utils.data_pipeline.provides(
        "tokens_list", "tokens_bos", "tokens_eos", "tokens"
    )
    def text_pipeline(wrd):
        '''
        ASR pipeline
        Parameters
        ----------
        wrd : The word contained in the CSV file

        Returns
        tokens_list : the tokenized word list
        token_bos : the tokenized word begining with BOS tag (thus sentence shifted to the right)
        token_eos : the tokenized word ending with EOS tag
        tokens :  the tokenized word tensor
        -------

        '''
        tokens_list = tokenizer.sp.encode_as_ids(wrd)
        yield tokens_list
        tokens_bos = torch.LongTensor([hparams["bos_index"]] + (tokens_list))
        yield tokens_bos
        tokens_eos = torch.LongTensor(tokens_list + [hparams["eos_index"]])
        yield tokens_eos
        tokens = torch.LongTensor(tokens_list)
        yield tokens

    sb.dataio.dataset.add_dynamic_item(datasets, text_pipeline)

    # 4. Define dependency pipeline

    @sb.utils.data_pipeline.takes("wrd", "pos", "gov", "dep")
    @sb.utils.data_pipeline.provides("wrd", "pos_tensor", "govDep2label", "depDep2Label")
    def dep2label_pipeline(wrd, poss, gov, dep):
        '''
        The dependecy parsing pipeline.
        Parameters
        ----------
        wrd : the raw word
        poss: the part of speech in the csv file
        gov : the gov/head label in the csv file
        dep : the syntactic function in the csv file

        Returns
        wrd: the raw word
        pos_list : the tensor of labeled part of speech
        govDep2label: the relative encoding of head/gov labeled
        depDep2Label: the labeled syntactic function
        -------

        '''
        yield wrd
        # 3 task is POS tagging
        try:
            pos_list = torch.LongTensor([label_alphabet[2].get(p) for p in poss.split(" ")])
        except TypeError:
            print(poss)
        yield pos_list
        fullLabel = relPos.encodeFromList(
            [w for w in wrd.split(" ")],
            [p for p in poss.split(" ")],
            [g for g in gov.split(" ")],
            [d for d in dep.split(" ")])
        # first task is gov pos and relative position
        try:
            govDep2label = torch.LongTensor([label_alphabet[0].get(fl.split("\t")[-1].split("{}")[0]) for fl in fullLabel])
            yield govDep2label
        except TypeError as e:
            print(wrd)
            print([fl.split("\t")[-1].split("{}")[0] for fl in fullLabel])
            print([label_alphabet[0].get(fl.split("\t")[-1].split("{}")[0]) for fl in fullLabel])
            raise TypeError() from e
        # second task is dependency type
        depDep2Label = torch.LongTensor([label_alphabet[1].get(fl.split("{}")[1]) for fl in fullLabel])
        yield depDep2Label

    sb.dataio.dataset.add_dynamic_item(datasets, dep2label_pipeline)

    # 4. Set output:
    sb.dataio.dataset.set_output_keys(
        datasets,
        ["id", "sig", "tokens_bos", "tokens_eos", "tokens", "wrd", "pos_tensor", "govDep2label", "depDep2Label"],
    )
    return train_data, valid_data, test_data


def build_label_alphabet(path_encoded_train):
    label_gov = dict()
    label_dep = dict()
    label_pos = dict()
    with open(path_encoded_train, "r", encoding="utf-8") as inputFile:
        for line in inputFile:
            field = line.split("\t")
            if len(field) > 1:
                fullLabel = field[-1]
                labelSplit = fullLabel.split("{}")
                govLabel = labelSplit[0]
                if govLabel not in label_gov:
                    label_gov[govLabel] = len(label_gov)
                depLabel = labelSplit[-1].replace("\n", "")
                if depLabel not in label_dep:
                    label_dep[depLabel] = len(label_dep)
                pos = field[1]
                if pos not in label_pos:
                    label_pos[pos] = len(label_pos)
    for pos_key in label_pos:
        for i in range(1,20):
            key=f"{i}@{pos_key}"
            if "+"+key not in label_gov.keys():
                label_gov["+"+key] = len(label_gov)
            if "-"+key not in label_gov.keys():
                label_gov["-"+key] = len(label_gov)
    label_gov["-1@INSERTION"]= len(label_gov)
    label_gov["-1@DELETION"]= len(label_gov)
    label_dep["INSERTION"]= len(label_dep)
    label_dep["DELETION"] = len(label_dep)
    label_pos["INSERTION"]= len(label_pos)
    return [label_gov, label_dep, label_pos]


relPos = RelPosEncoding()


def get_id_from_CoNLLfile(path):
    '''
    Get the sentence id from the conll file in the order
    Will be used to write in the same order for comparaison sakes.
    '''
    sent_id = []
    with open(path, "r", encoding="utf-8") as fin:
        for line in fin:
            if line.startswith("# sent_id"):
                field = line.split("=")
                sent_id.append(field[1].replace(" ", "").replace("\n", ""))
    return sent_id

def build_reverse_alphabet(alphabet):
    reverse=[]
    for alpha in alphabet:
        reverse.append({item: key for (key, item) in alpha.items()})
    return reverse

if __name__ == "__main__":
    wandb.init(group="Audio")
    path_encoded_train = "all.seq"  # For alphabet generation
    label_alphabet = build_label_alphabet(path_encoded_train)
    reverse_label_alphabet = build_reverse_alphabet(label_alphabet)
    print(label_alphabet)
    # Load hyperparameters file with command-line overrides
    hparams_file, run_opts, overrides = sb.parse_arguments(sys.argv[1:])
    with open(hparams_file) as fin:
        hparams = load_hyperpyyaml(fin, overrides) 

    os.environ["PATH"]+=hparams["PATH_TO_SCLITE"] # append path to sclite binaries. Needed if you train on server with resource manager like SLURM or OAR.
    print(os.environ["PATH"])
    goldPath_CoNLLU_dev = hparams["dev_gold_conllu"]
    goldPath_CoNLLU_test = hparams["test_gold_conllu"]
    dev_gold_trans = goldPath_CoNLLU_dev + "_trans"
    test_gold_trans = goldPath_CoNLLU_test + "_trans"
    conll2trans(goldPath_CoNLLU_dev, dev_gold_trans)
    conll2trans(goldPath_CoNLLU_test, test_gold_trans)
    Evaluator = evaluate.dep2LabelDecoder(label_alphabet)

    # If distributed_launch=True then
    # create ddp_group with the right communication protocol
    sb.utils.distributed.ddp_init_group(run_opts)

    # Dataset preparation (parsing CommonVoice)
    from cefcOrfeo_prepare import prepare_cefcOrfeo  # noqa

    # Create experiment directory
    sb.create_experiment_directory(
        experiment_directory=hparams["output_folder"],
        hyperparams_to_save=hparams_file,
        overrides=overrides,
    )

    # Due to DDP, we do the preparation ONLY on the main python process
    run_on_main(
        prepare_cefcOrfeo,
        kwargs={
            "data_folder": hparams["data_folder"],
            "save_folder": hparams["save_folder"],
            "train_tsv_file": hparams["train_tsv_file"],
            "dev_tsv_file": hparams["dev_tsv_file"],
            "test_tsv_file": hparams["test_tsv_file"],
            "accented_letters": hparams["accented_letters"],
            "skip_prep": hparams["skip_prep"],
        },
    )

    # Defining tokenizer and loading it
    tokenizer = SentencePiece(
        model_dir=hparams["save_folder"],
        vocab_size=hparams["output_neurons"],
        annotation_train=hparams["train_csv"],
        annotation_read="wrd",
        model_type=hparams["token_type"],
        character_coverage=hparams["character_coverage"],
    )

    # Create the datasets objects as well as tokenization and encoding :-D
    train_data, valid_data, test_data = dataio_prepare(hparams, tokenizer)
    
    # Trainer initialization
    asr_brain = ASR(
        modules=hparams["modules"],
        hparams=hparams,
        run_opts=run_opts,
        checkpointer=hparams["checkpointer"],
    )
    asr_brain.count_param_module()

    # Adding objects to trainer.
    # doing this to avoid overwriting the class constructor
    asr_brain.tokenizer = tokenizer
    asr_brain.d_oracle = DynamicOracle(label_alphabet,reverse_label_alphabet)
    asr_brain.is_training_syntax = False
    asr_brain.Evaluator = Evaluator
    #Diverse information on the data such as PATH and order of sentences.
    asr_brain.goldPath_CoNLLU_dev = goldPath_CoNLLU_dev
    asr_brain.goldPath_CoNLLU_test = goldPath_CoNLLU_test
    print(asr_brain.goldPath_CoNLLU_dev)
    asr_brain.dev_gold_trans = goldPath_CoNLLU_dev + "_trans"
    asr_brain.test_gold_trans = goldPath_CoNLLU_test + "_trans"
    asr_brain.dev_order = get_id_from_CoNLLfile(goldPath_CoNLLU_dev)
    asr_brain.test_order = get_id_from_CoNLLfile(goldPath_CoNLLU_test)
    
    # Training
    try:
        asr_brain.fit(
            asr_brain.hparams.epoch_counter,
            train_data,
            valid_data,
            train_loader_kwargs=hparams["dataloader_options"],
            valid_loader_kwargs=hparams["test_dataloader_options"],
        )
    except  RuntimeError as e:  # Memory Leak
        import gc

        for obj in gc.get_objects():
            try:
                if torch.is_tensor(obj) or (hasattr(obj, 'data') and torch.is_tensor(obj.data)):
                    pass
                    #print(type(obj), obj.size())
            except:
                pass
        raise RuntimeError() from e
    
    # Test
    
    asr_brain.hparams.wer_file = hparams["output_folder"] + "/wer_test.txt"
    asr_brain.evaluate(
        test_data,
        min_key="WER",
        test_loader_kwargs=hparams["test_dataloader_options"],
    )
    
    #transcribe
    
    asr_brain.transcribe_dataset(dataset=test_data, # Must be obtained from the dataio_function
    min_key="WER",
    loader_kwargs=hparams["test_dataloader_options"],)
    
