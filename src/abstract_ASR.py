import speechbrain as sb
import torch
import logging
import os

from torch.utils.data import DataLoader
from tqdm import tqdm

from dep2label.encodings import RelPosEncoding
import evaluate
from conll2trans import conll2trans
import subprocess
import wandb
from speechbrain.utils.edit_distance import wer_details_for_batch
from speechbrain.utils.data_utils import undo_padding
import tempfile
INTRA_EPOCH_CKPT_FLAG = "brain_intra_epoch_ckpt"
class ABSTRACT_ASR(sb.core.Brain):
    '''
    This class is used to factorized common behavior of the different version of wav2tree
    '''
    def on_fit_start(self):
        """Gets called at the beginning of ``fit()``, on multiple processes
        if ``distributed_count > 0`` and backend is ddp.
        Default implementation compiles the jit modules, initializes
        optimizers, and loads the latest checkpoint to resume training.
        """
        # Run this *after* starting all processes since jit modules cannot be
        # pickled.
        self._compile_jit()

        # Wrap modules with parallel backend after jit
        self._wrap_distributed()

        # Initialize optimizers after parameters are configured
        self.init_optimizers()

        # Load latest checkpoint to resume training if interrupted
        if self.checkpointer is not None:
            ckpt = self.checkpointer.recover_if_possible( # Only modification is : load which module are activated from checkpoint
                device=torch.device(self.device)
            )
            if ckpt is not None:
                self.is_training_syntax=ckpt.meta["is_training_syntax"]


    def on_evaluate_start(self, max_key=None, min_key=None):
        """Gets called at the beginning of ``evaluate()``
        Default implementation loads the best-performing checkpoint for
        evaluation, based on stored metrics.
        Arguments
        ---------
        max_key : str
            Key to use for finding best checkpoint (higher is better).
            By default, passed to ``self.checkpointer.recover_if_possible()``.
        min_key : str
            Key to use for finding best checkpoint (lower is better).
            By default, passed to ``self.checkpointer.recover_if_possible()``.
        """
        # Recover best checkpoint for evaluation
        if self.checkpointer is not None:
            ckpt = self.checkpointer.recover_if_possible(
                max_key=max_key,
                min_key=min_key,
                device=torch.device(self.device),
            )
            if ckpt is not None:
                self.is_training_syntax=ckpt.meta["is_training_syntax"]

    def _save_intra_epoch_ckpt(self):
        """Saves a CKPT with specific intra-epoch flag."""
        self.checkpointer.save_and_keep_only(
            end_of_epoch=False,
            num_to_keep=1,
            ckpt_predicate=lambda c: INTRA_EPOCH_CKPT_FLAG in c.meta,
            meta={INTRA_EPOCH_CKPT_FLAG: True, "is_training_syntax": self.is_training_syntax},
            verbosity=logging.DEBUG,
        )

    def fit_batch(self, batch):
        """Train the parameters given a single batch in input"""
        if self.auto_mix_prec:

            self.wav2vec_optimizer.zero_grad()
            self.model_optimizer.zero_grad()

            with torch.cuda.amp.autocast():
                outputs = self.compute_forward(batch, sb.Stage.TRAIN)
                loss = self.compute_objectives(outputs, batch, sb.Stage.TRAIN)

            self.scaler.scale(loss).backward()
            self.scaler.unscale_(self.wav2vec_optimizer)
            self.scaler.unscale_(self.model_optimizer)

            if self.check_gradients(loss):
                self.scaler.step(self.wav2vec_optimizer)
                self.scaler.step(self.adam_optimizer)

            self.scaler.update()
        else:
            outputs = self.compute_forward(batch, sb.Stage.TRAIN)

            loss = self.compute_objectives(outputs, batch, sb.Stage.TRAIN)
            loss.backward()
            if self.check_gradients(loss):
                self.wav2vec_optimizer.step()
                self.model_optimizer.step()

            self.wav2vec_optimizer.zero_grad()
            self.model_optimizer.zero_grad()

        return loss.detach()

    def evaluate_batch(self, batch, stage):
        """Computations needed for validation/test batches"""
        predictions = self.compute_forward(batch, stage=stage)
        with torch.no_grad():
            loss = self.compute_objectives(predictions, batch, stage=stage)
        return loss.detach()

    def on_stage_start(self, stage, epoch):
        """Gets called at the beginning of each epoch"""
        if stage != sb.Stage.TRAIN:
            self.cer_metric = self.hparams.cer_computer()
            self.wer_metric = self.hparams.error_rate_computer()
            self.dep2Label_metrics = self.hparams.dep2Label_computer()
            self.gov2Label_metrics = self.hparams.gov2Label_computer()

    def on_stage_end(self, stage, stage_loss, epoch):
        """Gets called at the end of an epoch."""
        # Compute/store important stats
        stage_stats = {"loss": stage_loss}
        if stage == sb.Stage.TRAIN:
            self.train_stats = stage_stats
        else:
            stage_stats["CER"] = self.cer_metric.summarize("error_rate")
            stage_stats["WER"] = self.wer_metric.summarize("error_rate")
            # Activate syntax module for training once ASR is good enough
            if self.is_training_syntax:
                if stage == sb.Stage.VALID:
                    st = self.hparams.dev_output_conllu
                    goldPath_CoNLLU = self.goldPath_CoNLLU_dev
                    gold_trans = self.dev_gold_trans
                    order = self.dev_order
                else:
                    st = self.hparams.test_output_conllu
                    goldPath_CoNLLU = self.goldPath_CoNLLU_test
                    gold_trans = self.test_gold_trans
                    order = self.test_order
                print(f"writing dev file in {st}")
                self.Evaluator.writeToCoNLLU(st, order)
                #transcriptPath = st + "_trans"
                transcript_tmp_file = tempfile.NamedTemporaryFile()
                transcriptPath = transcript_tmp_file.name
                conll2trans(st, transcriptPath) 
                sclite_command = ["sclite", "-F", "-i", "wsj", "-r", gold_trans,
                        "-h", transcriptPath, "-o", "sgml"]  # create file named transcriptPath.sgml
                #sclite_command = ["sclite", "-F", "-i", "wsj", "-r", gold_trans,
                #        "-h", transcriptPath, "-o", "sgml"
                #    ,"-O","/home/getalp/pupiera/thesis/endtoend_asr_multitask/src/ASRDEP2LABEL/"]  # create file named transcriptPath.sgml
                subprocess.run(sclite_command,
                               cwd=os.path.dirname(os.path.abspath(__file__)),check=True)
                transcript_tmp_file.close()
                metrics_dict = self.Evaluator.evaluateCoNLLU(goldPath_CoNLLU, st, f"{transcriptPath}.sgml")
                stage_stats["LAS"] = metrics_dict["LAS"].f1 * 100
                stage_stats["UAS"] = metrics_dict["UAS"].f1 * 100
                stage_stats["SER"] = metrics_dict["seg_error_rate"].precision * 100
                stage_stats["SENTENCES"] = metrics_dict["Sentences"].precision * 100
                stage_stats["Tokens"] = metrics_dict["Tokens"].precision * 100
                stage_stats["UPOS"] = metrics_dict["UPOS"].f1 * 100

                # stage_stats["DEP"] = self.dep2Label_metrics.summarize("F-score")
                # stage_stats["GOV"] = self.gov2Label_metrics.summarize("F-score")
            try:
                start_syntax_wer = self.hparams.start_syntax_WER
            except:
                start_syntax_wer = 50
            if stage_stats["WER"] < start_syntax_wer and not self.is_training_syntax:
                print("activating training syntax for next epoch")
                self.is_training_syntax = True

        # Perform end-of-iteration things, like annealing, logging, etc.
        if stage == sb.Stage.VALID:
            old_lr_model, new_lr_model = self.hparams.lr_annealing_model(
                stage_stats["loss"]
            )
            old_lr_wav2vec, new_lr_wav2vec = self.hparams.lr_annealing_wav2vec(
                stage_stats["loss"]
            )
            sb.nnet.schedulers.update_learning_rate(
                self.model_optimizer, new_lr_model
            )
            sb.nnet.schedulers.update_learning_rate(
                self.wav2vec_optimizer, new_lr_wav2vec
            )
            self.hparams.train_logger.log_stats(
                stats_meta={
                    "epoch": epoch,
                    "lr_model": old_lr_model,
                    "lr_wav2vec": old_lr_wav2vec,
                },
                train_stats=self.train_stats,
                valid_stats=stage_stats,
            )
            wandb_stats = {"epoch": epoch}
            wandb_stats = {**wandb_stats, **stage_stats}  # fuse dict
            wandb.log(wandb_stats)
            if self.is_training_syntax and "LAS" in stage_stats.keys():
                self.checkpointer.save_and_keep_only(
                        meta={"WER": stage_stats["WER"], "is_training_syntax": self.is_training_syntax, "LAS": stage_stats["LAS"]}, max_keys=["LAS"],
                )
            else:
                self.checkpointer.save_and_keep_only(
                        meta={"WER": stage_stats["WER"], "is_training_syntax": self.is_training_syntax, "LAS" : 0}, min_keys=["WER"],
                )
        elif stage == sb.Stage.TEST:
            self.hparams.train_logger.log_stats(
                stats_meta={"Epoch loaded": self.hparams.epoch_counter.current},
                test_stats=stage_stats,
            )
            with open(self.hparams.wer_file, "w") as w:
                self.wer_metric.write_stats(w)

    def init_optimizers(self):
        "Initializes the wav2vec2 optimizer and model optimizer"
        self.wav2vec_optimizer = self.hparams.wav2vec_opt_class(
            self.modules.wav2vec2.parameters()
        )
        self.model_optimizer = self.hparams.model_opt_class(
            self.hparams.model.parameters()
        )

        if self.checkpointer is not None:
            self.checkpointer.add_recoverable(
                "wav2vec_opt", self.wav2vec_optimizer
            )
            self.checkpointer.add_recoverable("modelopt", self.model_optimizer)
    def count_param_module(self):
        #print(type(self.modules))
        #print(self.modules)
        for key, value in self.modules.items():
            print(key)
            print(sum(p.numel() for p in value.parameters()))

    def transcribe_dataset(
            self,
            dataset,  # Must be obtained from the dataio_function
            min_key,  # We load the model with the lowest WER
            loader_kwargs,  # opts for the dataloading
            max_key=None
    ):
        '''
        Transcribe the dataset in the conllu format and transcription format with the WER.
        Parameters
        ----------
        dataset: The dataset to transribe
        min_key : the chosen checkpoint will minimise the value of this key (e;g : WER)
        loader_kwargs: The options of the dataloading
        max_key : the chosen checkpoint will maximise the value of this key (e.g : LAS)

        Returns
        -------

        '''

        # If dataset isn't a Dataloader, we create it.
        if not isinstance(dataset, DataLoader):
            loader_kwargs["ckpt_prefix"] = None
            dataset = self.make_dataloader(
                dataset, sb.Stage.TEST, **loader_kwargs
            )

        self.on_evaluate_start(min_key=min_key, max_key=max_key)  # We call the on_evaluate_start that will load the best model
        self.modules.eval()  # We set the model to eval mode (remove dropout etc)

        # Now we iterate over the dataset and we simply compute_forward and decode
        with torch.no_grad():
            WER=[]
            for batch in tqdm(dataset, dynamic_ncols=True):

                # Make sure that your compute_forward returns the predictions !!!
                # In the case of the template, when stage = TEST, a beam search is applied
                # in compute_forward().
                predictions = self.compute_forward(batch, stage=sb.Stage.TEST)
                p_ctc, wav_lens, p_depLabel, p_govLabel, p_posLabel, seq_len = predictions

                sequence = sb.decoders.ctc_greedy_decode(
                    p_ctc, wav_lens, blank_id=self.hparams.blank_index
                )   
                # We go from tokens to words.
                predicted_words = self.tokenizer(
                    sequence, task="decode_from_list"
                )
                for i, sent in enumerate(predicted_words):
                    for w in sent:
                        if w == '':
                            predicted_words[i] = [w for w in sent if w != '']
                            if len(predicted_words[i]) == 0:
                                predicted_words[i].append('EMPTY_ASR')
                tokens, tokens_lens = batch.tokens
                target_words = undo_padding(tokens, tokens_lens)
                target_words = self.tokenizer(target_words, task="decode_from_list")
                WER.append(wer_details_for_batch(batch.id, target_words, predicted_words))
                self.Evaluator.decode(p_depLabel, p_govLabel, predicted_words, p_posLabel, batch.id)
            st = self.hparams.test_output_conllu
            order = self.test_order
            self.Evaluator.writeToCoNLLU(st, order)
            WER = [item for sublist in WER for item in sublist]
            with open(self.hparams.transcript_file,"w", encoding="utf-8") as out:
                for e in WER:
                    out.write(f"{e['key']}\t{e['WER']}\n")
            print(f"MEAN WER : {sum([e['WER'] for e in WER]) /len(WER)}")

