import sys
import re
def conll2trans(inputpath,outpath):
    '''
    Transform the conllu file to transcriptions
    Parameters
    ----------
    inputpath : path of the conllu file to transcript
    outpath : path to the new transcript file

    Returns
    -------

    '''
    with open(inputpath,"r",encoding="utf-8") as input:
        with open(outpath,"w",encoding="utf-8") as out:
            sentence=[]
            sent_id=""
            for line in input:
                if line =="\n":
                    words = " ".join(sentence)
                    #words = re.sub(
                    #    "[^’'A-Za-z0-9À-ÖØ-öø-ÿЀ-ӿéæœâçèàûî]+", " ", words)
                    #words = words.replace("'", " ")
                    #words = words.replace("’", " ") 
                    #words = re.sub(" +", " ",words) #remove multiple space
                    out.write(f"{words.lower()} ({sent_id})"+"\n")
                    sentence=[]
                    continue
                if line.startswith("#"):
                    if "sent_id" in line:
                        sent_id = line.split("=")[1][1:-1] # remove space after =
                    continue
                field = line.split()
                sentence.append(field[1].lower()) # form is in column 1 of conll file

if __name__ == "__main__":
    conll = sys.argv[1]
    outpath = sys.argv[2]
    conll2trans(conll,outpath)
